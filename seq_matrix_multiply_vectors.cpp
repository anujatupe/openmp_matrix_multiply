#include <iostream>
#include <vector>

typedef std::vector <std::vector<int> > matrix;

using namespace std;


void print_vector_matrix(const matrix &m1) {
	int i, j;
	int m = m1.size();
  	int n = m1[0].size();
	for (i = 0; i < m; i++) {
		cout<<"\n";
		for (j = 0; j < n; j++) {
			cout<<m1[i][j]<<"\t";
		}
	}
}


matrix multiply_vector_matrices(const matrix &m1, const matrix &m2, int m, int n, int x, int y) {
	matrix m3;
	int i, j, k;
	for (i = 0; i < m; i++) {
		std::vector<int> temp_vec;
		int temp;
		for (j = 0; j < y; j++) {
			temp = 0;
			for (k = 0; k < x; k++) {
				temp = temp + (m1[i][k] * m2[k][j]);			
			}
			temp_vec.push_back(temp);
		}
		m3.push_back(temp_vec);
	}
	return m3;
}


matrix insert_elements_in_matrix(int m, int n) {
	matrix m1;
	int i, j;
	for(i = 0; i < m; i++){
		std::vector<int> temp_vec;
		int temp;
		for(j = 0; j < n; j++){
     		cin>>temp;
     		temp_vec.push_back(temp);
     	}
     	m1.push_back(temp_vec);
    }
    return m1;
}


int main() {
	int i, j, m, n, x, y;

	cout<<"\nEnter the number of rows and columns for matrix 1:";
	cin>>m>>n;
	cout<<"\nEnter the number of rows and columns for matrix 2:";
	cin>>x>>y;

	matrix m1, m2, m3;
	
	if (n == x) {
		cout<<"\nEnter the first matrix (matrix1): ";
		m1 = insert_elements_in_matrix(m, n);

		cout<<"\nEnter the second matrix (matrix2): ";
		m2 = insert_elements_in_matrix(x, y);

		cout<<"\nFirst Matrix is: ";
		print_vector_matrix(m1);

		cout<<"\n\nSecond Matrix is: ";
		print_vector_matrix(m2);

		m3 = multiply_vector_matrices(m1, m2, m, n, x, y);

		cout<<"\n\nFinal Matrix is: ";
		print_vector_matrix(m3);

	} else {
		cout<<"\n****ERROR: The number of columns of 1st matrix is not equal to the number of rows of the 2nd matrix";
	}
	
	return 0;
}