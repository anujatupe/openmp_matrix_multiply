#include <iostream>
#include <time.h>
#include <omp.h>
#include <cstdint>

#define SECINNANOSEC 1000000000L

using namespace std;

//int **matrix1, **matrix2, **matrix3;

void print_matrix(int **matrix, int m, int y) {
  int i, j;
  for (i = 0; i < m; i++) {
    cout<<"\n";
    for (j = 0; j < y; j++) {
      cout<<matrix[i][j]<<"\t";
    }
  }
}

void freeArray(int **matrix, int m) {
  int i;
  for (i = 0; i < m; ++i) {
    free(matrix[i]);
  }
  free(matrix);
}

int ** allocate_space_for_array(int m, int n) {
  int i;
  int **matrix;
  matrix = (int **) malloc(m * sizeof(int*)) ;
  for(i = 0; i<m; i++) {
    matrix[i] = (int *) malloc(n * sizeof(int));
  }
  return matrix;
}

int alg_matmul2D(int m, int n, int p, int** a, int** b, int** c)
{
  int i,j,k;
  #pragma omp parallel shared(a,b,c) private(i,j,k)
  {
    #pragma omp for  schedule(static)
    for (i=0; i<m; i=i+1){
      for (j=0; j<p; j=j+1){
        a[i][j]=0.;
        for (k=0; k<n; k=k+1){
          a[i][j]=(a[i][j])+((b[i][k])*(c[k][j]));
        }
      }
     }
  }
  return 0;
}


int main() {
  timespec start_time, end_time;
  int **matrix1, **matrix2, **matrix3;
  uint64_t diff_time;
  int i, j, m, n, x, y;
  //int k;

  cout<<"\nEnter the number of rows and columns in matrix 1:";
  cin>>m>>n;
  cout<<"\nEnter the number of rows and columns in matrix 2:";
  cin>>x>>y;

  if (n == x) {      
    matrix1 = allocate_space_for_array(m, n);
    matrix2 = allocate_space_for_array(x, y);
    matrix3 = allocate_space_for_array(m, y);

    cout<<"\nGenerating first matrix...";
    for(i = 0; i < m; i++){
      for(j = 0; j < n; j++){
        matrix1[i][j] = rand() % 10 + 1;
      }
    }

    cout<<"\nGenerating second matrix...";
    for(i = 0; i < x; i++){
      for(j = 0; j < y; j++){
        matrix2[i][j] = rand() % 10 + 1;
      }
    }

    clock_gettime(CLOCK_MONOTONIC, &start_time);
    alg_matmul2D(m, x, y, matrix3, matrix1, matrix2);
    //#pragma omp parallel shared(matrix3,matrix1,matrix2) private(i,j,k)
    //{
     //#pragma omp for  schedule(static)
      //for (i=0; i<m; i=i+1){
        //for (j=0; j<y; j=j+1){
          //matrix3[i][j]=0.;
          //for (k=0; k<x; k=k+1){
            //matrix3[i][j]=(matrix3[i][j])+((matrix1[i][k])*(matrix2[k][j]));
          //}
        //}
       //}
    //}
    clock_gettime(CLOCK_MONOTONIC, &end_time);

  } else {
    cout<<"\n****ERROR: The number of columns of 1st matrix is not equal to the number of rows of the 2nd matrix";
  }

  diff_time = SECINNANOSEC * (end_time.tv_sec - start_time.tv_sec) + end_time.tv_nsec - start_time.tv_nsec;
  cout<<"\n\n*** Time required parallel matrix multiplication: "<<(long long unsigned int) diff_time<<" nanoseconds ***\n\n";

  freeArray(matrix1, m);
  freeArray(matrix2, x);
  freeArray(matrix3, m);
   
  //free(matrix1);
  //free(matrix2);
  //free(matrix3);

  return 0;
}
