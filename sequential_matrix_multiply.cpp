#include <iostream>

using namespace std;

void multiply_matrices(int matrix1[][10], int matrix2[][10], int matrix3[][10], int m, int n, int x, int y) {
	int i, j, k;
	for (i = 0; i < m; i++) {
		for (j = 0; j < y; j++) {
			matrix3[i][j]=0;
			for (k = 0; k < x; k++) {
				matrix3[i][j] = matrix3[i][j] + (matrix1[i][k] * matrix2[k][j]);			
			}
			
		}
	}
}

void print_matrix(int matrix[][10], int m, int y) {
	int i, j;
	for (i = 0; i < m; i++) {
		cout<<"\n";
		for (j = 0; j < y; j++) {
			cout<<matrix[i][j]<<"\t";
		}
	}
}

int main() {
	int i, j, m, n, x, y;

	cout<<"\nEnter the number of rows in matrix 1:";
	cin>>m;
	cout<<"\nEnter the number of columns in matrix 1:";
	cin>>n;
	cout<<"\nEnter the number of rows in matrix 2:";
	cin>>x;
	cout<<"\nEnter the number of columns in matrix 2:";
	cin>>y;

	int matrix1[10][10], matrix2[10][10], matrix3[10][10];
	
	
	if (n == x) {
		cout<<"\nEnter the first matrix (matrix1): ";
		for(i = 0; i < m; i++){
			for(j = 0; j < n; j++){
	     		cin>>matrix1[i][j];
	     	}
	     }

		cout<<"\nEnter the second matrix (matrix2): ";
		for(i = 0; i < x; i++){
			for(j = 0; j < y; j++){
		    	cin>>matrix2[i][j];
			}
		}
		multiply_matrices(matrix1, matrix2, matrix3, m, n, x, y);	

		cout<<"\nFirst Matrix is: ";
		print_matrix(matrix1, m, n);

		cout<<"\n\nSecond Matrix is: ";
		print_matrix(matrix2, x, y);

		cout<<"\n\nFinal Matrix is: ";
		print_matrix(matrix3, m, y);
	} else {
		cout<<"\n****ERROR: The number of columns of 1st matrix is not equal to the number of rows of the 2nd matrix";
	}
	

		
	
	return 0;
}