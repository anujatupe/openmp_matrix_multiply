#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <pthread.h>
#include <string>
#include <cstring>
#include <time.h>
#include <fstream>
#include <cstdint>
#include <semaphore.h>

using namespace std;

#define BUFFERSIZE 10
#define LINESLIMIT 1000

struct element {
  vector<string> text;
  int start_line;
};

char *fname;
long no_threads;
long no_lines;
int producer_count;
char pattern[150];

sem_t slotsavail;
sem_t resavail;
pthread_mutex_t l1, l2, occurrences_lock, file_end_lock;
int in, out;
int total_occurences;
int is_end_of_file = 0;
element queue[BUFFERSIZE];


int get_number_of_lines(char *filename) {
  FILE *fp = fopen(filename,"r");
  char ch = 0;
  int lines = 0;
  while(!feof(fp)) {
    ch = fgetc(fp);
    if(ch == '\n') {
      lines++;
    }
  }
  //printf("The number of lines are: %d\n", lines);
  fclose(fp);
  return lines;
}


void initialize() {
  pthread_mutex_init(&l1, NULL);
  pthread_mutex_init(&l2, NULL);
  pthread_mutex_init(&file_end_lock, NULL);
  in = 0;
  out = 0;
  total_occurences = 0;
  producer_count = 0;
  sem_init(&slotsavail, 0, BUFFERSIZE);
  sem_init(&resavail, 0, 0);
}


void* producer(void *pthread_id) {

  ifstream filename(fname);
  string current_line;
  int temp_start_line;
  int count_line = 0;
  int i = 0;

  while(1) {
    vector<string> temp_text;
    count_line = 0;
    i = 0;
    
    sem_wait(&slotsavail);
    
    
    while((i < LINESLIMIT) && getline(filename, current_line)) {
	    temp_text.push_back(current_line);
	    i++;
	    count_line++;
	  }

	//cout<<"\n***********Produced thousand line:**********\n\n";
	
	// int j = 0;
	// for (j = 0; j <  temp_text.size(); j++){
	// 	cout<<"\n"<<temp_text[j];
	// }

	
    temp_start_line = producer_count * LINESLIMIT;
    // if (count_line != LINESLIMIT) {
    //   temp_start_line = ((producer_count - 1) * LINESLIMIT) + count_line ;
    // } else {
    //   temp_start_line = producer_count * count_line;
    // }

    queue[in].text = temp_text;
    queue[in].start_line = temp_start_line;

    in = (in + 1) % BUFFERSIZE;
    
    sem_post(&resavail);

    producer_count++; 

    if (filename.eof()) {
      pthread_mutex_lock(&file_end_lock);
      is_end_of_file = 1;
      pthread_mutex_unlock(&file_end_lock);
      cout<<"\n\n************************Breaking from producer loop***************";
      break;
    }
  }
}

int find_pattern_in_text(vector <string> &text, int start_line, long pid) {
  
  long line_count = 0;
  //long my_thread_id = (long) thread_id;
  //long no_lines_limit;
  //long no_lines_for_every_thread = 0;
  //no_lines_limit = no_of_lines_per_thread;
  int occurrences = 0;
  int occurence_on_every_line = 0;
  int is_found = 0;
  int start;
  //int line_count_start_value = 0;

  
  for (line_count = 0; line_count < text.size(); line_count++){
    string::size_type start = 0;
    is_found = 0;
    occurence_on_every_line = 0;

    while ((start = text[line_count].find(pattern, start)) != string::npos) {
      is_found = 1;
      ++occurrences;
      ++occurence_on_every_line;
      start += strlen(pattern);
    }

    if (is_found == 1) {
      std::cout << "\n Thread "<<pid<<" - Found "<<occurence_on_every_line<<" occurences at Line " <<(start_line+line_count);
    }
  }
  

  //cout<<"\n*** Number of occurrences in thread "<<pid<<" are "<<occurrences<<" ***\n";
  return occurrences;
}

void *consumer (void *pthread_id) {
	long pid = (long) pthread_id;
	int start = 0;
	int occurrences = 0;
	int tmpout;
	int sem_curr_val;
	while(1) {

		// sem_getvalue(&resavail, &sem_curr_val);
		// pthread_mutex_lock(&file_end_lock);
		// if ((sem_curr_val <= 0) && (is_end_of_file == 1)) {
		// 	pthread_mutex_unlock(&file_end_lock);
		// 	break;
		// }
		

		vector<string> current_text;
		element current_queue_ele;
		start = 0;

		cout<<"\nConsumer is waiting for items......";
		sem_wait(&resavail);	
		
		pthread_mutex_lock(&l2);
		
		tmpout = out;
		out = (out + 1) % BUFFERSIZE;
		current_queue_ele = queue[tmpout];

		pthread_mutex_unlock(&l2);
		
		

		// current_text = current_queue_ele.text;
		// cout<<"\nBlock start is: "<<current_queue_ele.start_line;
		// int j = 0;
		// for (j = 0; j <  current_text.size(); j++){
		//   cout<<"\n"<<current_text[j];
		// }

		// cout<<"\n\n";
		
		occurrences = find_pattern_in_text(current_queue_ele.text, current_queue_ele.start_line, pid);
		
		pthread_mutex_lock(&occurrences_lock);
		total_occurences += occurrences;
		pthread_mutex_unlock(&occurrences_lock);
		
		sem_post(&slotsavail);
	}
}


int main(int argc, char *argv[]) {
  pthread_t consumer_handles[100];
  pthread_t producer_handle;
  // pthread_t c_handle;
  // pthread_t c_handle2;
  // pthread_t c_handle3;
  fname = argv[1];
  
  long number_of_consumers = atoi(argv[2]);
  
  strcpy(pattern, argv[3]);
  initialize();

  long producer_id = 1;
  long i = 0;
  // long i2 = 1;
  // long i3 = 2;
  pthread_create(&producer_handle, NULL, producer, (void*) producer_id);

  // pthread_create(&c_handle, NULL, consumer, (void*) i);
  // pthread_create(&c_handle2, NULL, consumer, (void*) i2);
  // pthread_create(&c_handle3, NULL, consumer, (void*) i3);

   for (i = 0; i < number_of_consumers; i++) {
     pthread_create(&consumer_handles[i], NULL, consumer, (void*) i);
   }

  

  
  pthread_join(producer_handle, NULL);
  for (i = 0; i < number_of_consumers; i++) {
    pthread_join(consumer_handles[i], NULL);
  }
  // pthread_join(c_handle, NULL);
  // pthread_join(c_handle2, NULL);
  // pthread_join(c_handle3, NULL);

  pthread_mutex_lock(&occurrences_lock);
  cout<<"\n\n*************** TOTAL OCCURENCES *************: "<<total_occurences;
  pthread_mutex_unlock(&occurrences_lock);

  return 0;
}